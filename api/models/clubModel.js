const mongoose = require('mongoose')
const schema = mongoose.Schema


const clubModel = new schema({
    name :  String , 
    country : String,
    league : String,
    foundation: Number,
    stadium: String
})

const club = mongoose.model('club', clubModel)

module.exports = club
