const mongoose = require('mongoose')
const schema = mongoose.Schema


const playerModel = new schema({
    name: String,
    age: Number,
    nationality: String,
    position: String,
    club: String
})

const player = mongoose.model('player', playerModel)

module.exports = player