const mongoose = require('mongoose')
const schema = mongoose.Schema


const leagueModel = new schema({
    name :  String , 
    country : String,
    division : String,
    club_quantity: Number
})

const league = mongoose.model('league', leagueModel)

module.exports = league
