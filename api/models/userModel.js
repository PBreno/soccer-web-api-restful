const mongoose = require('mongoose')
const schema = mongoose.Schema


const userModel = new schema({
    name :  String , 
    cpf: String,
    password : String
})

const users = mongoose.model('users', userModel)

module.exports = users
