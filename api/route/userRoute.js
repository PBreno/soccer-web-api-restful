const express  = require ('express')
const router = express.Router()
let userdata = require('../data/userDATA')

router.get('/', (req, res)=>{
    
    userdata.list(req, res)
})

router.get('/:id', (req,res) =>{

    let id = req.params.id

    userdata.listbyID(id,req,res)

})

router.post('/', (req,res)=>{

    userdata.insert(req,res)
})

router.put('/:id', (req,res)=>{
    
    let id = req.params.id

    userdata.update(id,req, res)
})

router.delete('/:id', (req,res) =>{

    let id= req.params.id

    userdata.delete(id, req, res)
})

module.exports= router