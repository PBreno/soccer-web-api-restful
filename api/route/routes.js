const express = require('express')
const router = express.Router()
let  clubRoute = require('./clubRoute')
let leagueRoute = require('./leagueRoute')
let playerRoute = require ('./playerRoute')
let userRoute = require ('./userRoute')

// Routas de cada EndPoint
router.use('/clubs', clubRoute)
router.use('/leagues', leagueRoute)
router.use('/players',playerRoute)
router.use('/users',userRoute)

module.exports = router