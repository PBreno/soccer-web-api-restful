const express  = require ('express')
const router = express.Router()
let clubdata = require('../data/clubDATA')

router.get('/', (req, res)=>{
    
    clubdata.list(req, res)
})

router.get('/:id', (req,res) =>{

    let id = req.params.id

    clubdata.listbyID(id,req,res)

})

router.post('/', (req,res)=>{

    clubdata.insert(req,res)
})

router.put('/:id', (req,res)=>{
    
    let id = req.params.id

    clubdata.update(id,req, res)
})

router.delete('/:id', (req,res) =>{

    let id= req.params.id

    clubdata.delete(id, req, res)
})

module.exports= router