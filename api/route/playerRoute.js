const express  = require ('express')
const router = express.Router()
let playerdata = require('../data/playerDATA')

router.get('/', (req, res)=>{
    
    playerdata.list(req, res)
})

router.get('/:id', (req,res) =>{

    let id = req.params.id

    playerdata.listByID(id,req,res)

})

router.post('/', (req,res)=>{

    playerdata.insert(req,res)
})

router.put('/:id', (req,res)=>{
    
    let id = req.params.id

    playerdata.update(id,req, res)
})

router.delete('/:id', (req,res) =>{

    let id= req.params.id

    playerdata.delete(id, req, res)
})

module.exports= router