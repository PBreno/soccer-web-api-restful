const express  = require ('express')
const router = express.Router()
let leaguedata = require('../data/leagueDATA')

router.get('/', (req, res)=>{
    
    leaguedata.list(req, res)
})

router.get('/:id', (req,res) =>{

    let id = req.params.id

    leaguedata.listbyID(id,req,res)

})

router.post('/', (req,res)=>{

    leaguedata.insert(req,res)
})

router.put('/:id', (req,res)=>{
    
    let id = req.params.id

    leaguedata.update(id,req, res)
})

router.delete('/:id', (req,res) =>{

    let id= req.params.id

    leaguedata.delete(id, req, res)
})

module.exports= router