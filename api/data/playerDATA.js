const connection = require('./authentication')
let playerModel = require('../models/playerModel')


exports.list = async function (req, res, next) {

    try {

        await playerModel.find({}, function (err, listModel) {

            if (err)
                res.status(404).send('There is no element to list!')
            else
                res.status(200).send(listModel)

        }).catch(next)

    } catch (error) {
        console.log(error)
    }
}

exports.listByID = async function (ID, req, res, next) {

    try {

        await playerModel.findById({ _id: ID }, function (err, listByIDModel) {

            if(listByIDModel == null)
                res.status(404).send(`The ID: ${ID}, does not match with any element. Please, be sure that this ID exits!`)    
            
            else
                res.status(200).send(listByIDModel)

        }).catch(next)

    } catch (error) {
        console.log(error)
    }
}

exports.insert = async function (req, res, next) {

    try {

        await playerModel.create(req.body).then(function (insertModel) {

            res.status(200).send(insertModel)
            console.log('Inserted!')

        }).catch(next)
    } catch (error) {
        res.status(400).send(error)
    }
}

exports.update = async function (ID, req, res, next) {

    try {

        await playerModel.findByIdAndUpdate({ _id: ID }, req.body).then(function () {
            playerModel.findById({ _id: ID }).then(function (err, updateModel) {

                if(!err)
                    res.status(404).send(`The ID: ${ID}, does not match with any element. Please, be sure that this ID exits!`)

                else{

                    res.status(200).send(updateModel)
                    console.log('Updated!')
                } 

            })
        }).catch(next)

    } catch (error) {
        res.status(400).send(error)
    }
}

exports.delete = async function (ID, req, res, next) {

    try {

        await playerModel.findByIdAndDelete({ _id: ID }).then(function (err, deleteModel) {

            if (!err)
                res.status(404).send(`The ID: ${ID}, does not match with any element. Please, be sure that this ID exits!`)

            else {

                res.status(200).send(deleteModel)             
                console.log('Delected!')
             }
        }).catch(next)

    } catch (error) {
        res.status(400).send(error)
    }
}