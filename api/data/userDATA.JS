let userModel = require('../models/userModel')


exports.list = async function (req, res, next) {

    try {


        await userModel.find({}, function (err, listModel) {

            if (err)
                res.status(404).send('There is no Users to list!')
            else
                res.status(200).send(listModel)


        }).catch(next)

    } catch (error) {
        console.log(error)
    }
}

exports.listbyID = async function (ID, req, res, next) {

    try {

        await userModel.findById({ _id: ID }, function (err, listByIDModel) {

            if (listByIDModel == null)
                res.status(404).send('User not found. Be sure that this ID exist!')

            else 
                res.send(listByIDModel)

        }).catch(next)

    } catch (error) {
        console.log(error)
    }
}

exports.insert = async function insert(req, res, next) {

    try {

        await userModel.create(req.body).then(function (insertModel) {

            res.status(200).send(insertModel)
            console.log('Inserted!')

        }).catch(next)

    } catch (error) {
        res.status(400).send(error)
    }
}

exports.update = async function (ID, req, res, next) {

    try {

        await userModel.findByIdAndUpdate({ _id: ID }, req.body).then(function () {
            userModel.findById({ _id: ID }).then(function (err,updateModel) {

                if(!err)
                    res.status(404).send('User not found. Be sure that this ID exist!')

                else{

                    res.status(200).send(updateModel)
                    console.log('Updated!')
                } 

            })
        }).catch(next)

    } catch (error) {
        res.status(400).send(error)
    }
}

exports.delete = async function (ID, req, res, next) {

    try {

        await userModel.findByIdAndDelete({ _id: ID }).then(function (err, deleteModel) {


            if (!err){
                res.status(404).send('User not found. Be sure that this ID exist!')
            }
            else {

                res.status(200).send(deleteModel)
                console.log('Delected!')
            }

        }).catch(next)

    } catch (error) {
        res.status(400).send(error)
    }

}
