const connection = require('./authentication')
let clubModel = require('../models/clubModel')

exports.list = async function (req, res, next) {

    try {

        await clubModel.find({}, function (err, listModel) {

            if (err) 
                res.status(404).send('There is no element to list!')
              
            else
                res.status(200).send(listModel)

        }).catch(next)


    } catch (error) {
        console.log(error)
    }
}

exports.listbyID = async function (ID, req, res, next) {

    try {

        
        await clubModel.findById({ _id: ID }, function (err, listByIDModel) {

            if(listByIDModel == null)
                res.status(404).send('Element not found. Be sure that this ID exist!')    
            else
                res.status(200).send(listByIDModel)

        }).catch(next)
        
    } catch (error) {
        console.error(error)
    }
}

exports.insert = async function (req, res, next) {

    try {

        await clubModel.create(req.body).then(function (insertModel) {


            res.status(200).send(insertModel)
            console.log('Inserted!')

        }).catch(next)
    } catch (error) {
        res.status(400).send(error)
    }
}

exports.update = async function (ID, req, res, next) {

    try {

        await clubModel.findByIdAndUpdate({ _id: ID }, req.body).then(function () {
            clubModel.findById({ _id: ID }).then(function (err,updateModel) {


                if(!err)
                    res.status(404).send('Element not found!')

                else{

                    res.status(200).send(updateModel)
                    console.log('Updated!')
                } 
                    

            })
        }).catch(next)

    } catch (error) {
        res.status(400).send(error)
    }
}

exports.delete = async function (ID, req, res, next) {

    try {

        await clubModel.findByIdAndDelete({ _id: ID }).then(function (err, deleteModel) {


            if (!err)
                res.status(404).send('There is no Element in this document')

            else {

                res.status(200).send(deleteModel)
                console.log('Delected!')
            }
        }).catch(next)

    } catch (error) {
        res.status(400).send(error)
    }
}