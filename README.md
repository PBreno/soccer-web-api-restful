# Soccer Web API RESTful

About: This project is a development of a WEB API using node.js and express.js library.
Autor: Breno Pombo

## INSTALAÇÃO
// npm install mongoose express
## ENDPOINTS
League
 ### Recurso 1
 ### Modelo de Dado
 {
  name :  String , 
  country : String,
  division : String,
  player_quantity: String
 }
 ### GET
 #### http://localhost:3000/root/selectall/league
 #### Exemplo de resposta
 {
   "_id": "616f57648234c38f1fd40465",
   "name": "Barcelona",
   "country": "Spain",
   "league": " La Liga",
   "foundation": 1899,
   "stadium": "Camp Nou",
   "__v": 0
  },
 #### Erros
 #### Filtros 
 http://localhost:3000/root/selectbyid/league/616f57648234c38f1fd40465
 ### POST
 #### Requisição
 http://localhost:3000/root/create/league
 #### Exemplo de resposta
 {
  "name": "Bundesliga",
  "country": " Germany",
  "division": "First Division",
  "_id": "6170e333c03e8311115a967e",
  "__v": 0
}
 ### PUT
 #### Requisição
 http://localhost:3000/root/update/league/616f58a9bb3e2b4b74fd0460
 #### Exemplo de resposta
 {
   "name": "Famengo",
   "country": "Brazil",
   "league": "Serie A",
   "foundation": "1895",
   "stadium": "Maracanã"
}
 ### DELETE
 #### Requisição
 http://localhost:3000/root/remove/league/617089211f861fdc34be1be4
 #### Exemplo de resposta
{
   "name": "Famengo",
   "country": "Brazil",
   "league": "Serie A",
   "foundation": "1895",
   "stadium": "Maracanã"
}

## ENDPOINTS
player
 ### Recurso 1
 ### Modelo de Dado
 {
     name :  String , 
     country : String,
     league : String,
     foundation: Number,
     stadium: String
 }
 ### GET
 #### http://localhost:3000/root/selectall/player
 #### Exemplo de resposta
 {
   "_id": "616f57648234c38f1fd40465",
   "name": "Barcelona",
   "country": "Spain",
   "league": " La Liga",
   "foundation": 1899,
   "stadium": "Camp Nou",
   "__v": 0
  },
 #### Erros
 #### Filtros 
 http://localhost:3000/root/selectbyid/player/616f57c9224a7a72d7fced46
 ### POST
 #### Requisição
 http://localhost:3000/root/create/player
 #### Exemplo de resposta
 {
  "name": "Bayer de Munich",
  "country": "Germany",
  "league": "Bundesliga",
  "foundation": 1878,
  "stadium": "Benz Arena",
  "_id": "6170e58c713fa091124ce844",
  "__v": 0
}
 #### Erros
 ### PUT
 #### Requisição
 http://localhost:3000/root/update/league/170e58c713fa091124ce844
 #### Exemplo de resposta
 {
  "name": "Bayer de Munich",
  "country": "Germany",
  "league": "Bundesliga",
  "foundation": 1950,
  "stadium": "Bayer Arena",
  "_id": "6170e58c713fa091124ce844",
  "__v": 0
}
 #### Erros
 ### DELETE
 #### Requisição
 http://localhost:3000/root/remove/player/170e58c713fa091124ce844
 #### Exemplo de resposta
{
  "name": "Bayer de Munich",
  "country": "Germany",
  "league": "Bundesliga",
  "foundation": 1950,
  "stadium": "Bayer Arena",
  "_id": "6170e58c713fa091124ce844",
  "__v": 0
}

## ENDPOINTS
player
 ### Recurso 1
 ### Modelo de Dado
 {
     name: String,
     age: Number,
     nationality: String,
     position: String,
     club: String
 }
 ### GET
 #### http://localhost:3000/root/selectall/player
 #### Exemplo de resposta
 {
     "_id": "6170876b4077c1a746af6b62",
     "name": "Bruno Fernandes",
     "age": 27,
     "nationality": "Portugal",
     "position": "Meio-Campo",
     "club": "Manchester United",
     "__v": 0
  },
 #### Erros
 #### Filtros 
 http://localhost:3000/root/selectbyid/player/616f57c9224a7a72d7fced46
 ### POST
 #### Requisição
 http://localhost:3000/root/create/player
 #### Exemplo de resposta
 {
  "name": "Frankie",
  "age": 24,
  "nationality": "Holandês",
  "position": "Meia",
  "club": "Barcelona",
  "_id": "6170e70a713fa091124ce84c",
  "__v": 0
}
 #### Erros
 ### PUT
 #### Requisição
 http://localhost:3000/root/update/league/170e58c713fa091124ce844
 #### Exemplo de resposta
 {
  "name": "Frankie de Jong",
  "age": 24,
  "nationality": "Holandês",
  "position": "Meia",
  "club": "Barcelona",
  "_id": "6170e70a713fa091124ce84c",
  "__v": 0
}
 #### Erros
 ### DELETE
 #### Requisição
 http://localhost:3000/root/remove/player/6170e70a713fa091124ce84c
 #### Exemplo de resposta
{
  "name": "Frankie",
  "age": 24,
  "nationality": "Holandês",
  "position": "Meia",
  "club": "Barcelona",
  "_id": "6170e70a713fa091124ce84c",
  "__v": 0
}
## AUTENTICAÇÃO DATABASE
login: dbabp
senha: soccerAPI
