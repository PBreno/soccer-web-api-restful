const express = require ('express')
const app = express()
const routes = require('./api/route/routes')
const port = 3000

app.use(express.json()) // Trata todo o corpo de mensagem JSON
app.use('/api', routes)

app.listen(port, console.log(`I´m listennig in port ${port}...`))